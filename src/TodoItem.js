import React from 'react'
import Checkbox from 'material-ui/Checkbox'
import IconButton from 'material-ui/IconButton'
import CheckIcon from 'material-ui/svg-icons/navigation/check'
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'
import ExpandLessIcon from 'material-ui/svg-icons/navigation/expand-less'
import Divider from 'material-ui/Divider'
import UnchckedIcon from 'material-ui/svg-icons/toggle/radio-button-unchecked'

const TodoItem = (props) => {
  const {item, onCheck, handleCollaps, openedItemId} = props
  const style = {
    padding:15,
    opacity: item.concluido ? '0.5' : '1',
    textDecoration: item.concluido ? 'line-through' : 'none',
    transition: 'all 0.5s',
    position: 'relative',
    collaps: {
      maxHeight: item.imagem && item.id === openedItemId ? '500px' : '0px',
      overflow: 'hidden',
      transition: 'all 0.8s'
    },
    collapsIcon: {
      position: 'absolute',
      zIndex: '9999',
      top: 2,
      right: 2,
      display: item.imagem ? 'block' : 'none'
    }
  }
  return (
    <div>
      <div style={style}>
        <Checkbox
          checked={item.concluido}
          onCheck={onCheck}
          label={item.nome}
          uncheckedIcon={<UnchckedIcon/>}
          checkedIcon={<CheckIcon/>}
        />
        <IconButton onClick={() => handleCollaps(item.id)} style={style.collapsIcon}>
          {item.id === openedItemId ? <ExpandLessIcon/> : <ExpandMoreIcon/> }
        </IconButton>
        <div style={style.collaps}>
          <img width="100%" src={item.imagem} alt="" style={{marginTop: 10}}/>
        </div>
      </div>
      <Divider/>
    </div>
  )
}

export default TodoItem
