import React from 'react'
import {Paper, TextField, IconButton} from 'material-ui'
import AddAPhoto from 'material-ui/svg-icons/image/add-a-photo'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

class AddItem extends React.Component {
  
  state = {
    itemNameInput: '',
    imagemUrlInput: '',
    selectedImagemUrlInput: '',
    open: false
  }
  
  handleOpen = () => {
    this.setState({open: true})
  }

  handleClose = () => {
    this.setState({open: false})
  }
  
  handleImageUrlInput = (event) => {
    this.setState({imagemUrlInput: event.target.value})
  }
  
  handleAddImagem = () => {
    
    if(!this.state.itemNameInput && !this.state.imagemUrlInput) return;
    
    const {onChange} = this.props
    
    this.setState({selectedImagemUrlInput: this.state.imagemUrlInput})
    
    const item = {
      nome: this.state.itemNameInput,
      imagem: this.state.imagemUrlInput
    }
    
    onChange(item)
    
    this.setState({itemNameInput: '', imagemUrlInput: ''})
    this.handleClose()
  }
  
  handleItemNameInput = itemNameInput => this.setState({itemNameInput})
  
  handleAddItem = () => {
    
    if(!this.state.itemNameInput) return;
    
    const {onChange} = this.props
    const item = {
      nome: this.state.itemNameInput
    }
    onChange(item)
    this.setState({itemNameInput: ''})
  }
  
  render() {
    
    const {itemNameInput} = this.state
    
    const actions = [
      <FlatButton
        label="Cancelar"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Adicionar"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleAddImagem}
      />,
    ];
    
    return (
      <Paper zDepth={1} className='add-todo-item'>

        <Dialog
          title="Inserir tarefa com imagem."
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <TextField
            hintText="Nome da tarefa"
            fullWidth={true}
            onChange={(event) => this.handleItemNameInput(event.target.value)}
            value={this.state.itemNameInput}
          />
          <TextField
            hintText="http://"
            fullWidth={true}
            onChange={this.handleImageUrlInput}
            value={this.state.imagemUrlInput}
          />
        </Dialog>
      
        <TextField
          hintText="O que precisa ser feito?"
          underlineShow={false}
          className='field'
          fullWidth={true}
          onChange={(event) => this.handleItemNameInput(event.target.value)}
          value={itemNameInput}
          onKeyDown={(event) => event.keyCode === 13 ? this.handleAddItem() : null}
        />
        <IconButton
          tooltip="Adicionar foto"
          className='add-a-photo'
          onTouchTap={this.handleOpen}
        >
          <AddAPhoto/>
        </IconButton>
      </Paper>
    )
  }
}

export default AddItem
