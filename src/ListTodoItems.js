import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import TodoItem from './TodoItem'
import Paper from 'material-ui/Paper'

class ListTodoItems extends React.Component {
  state = {openedItemId: null}
  
  handleOpenItem = itemId => {
    this.setState({openedItemId: itemId === this.state.openedItemId ? null : itemId})
  }
  
  render() {
    const {items, onCheck} = this.props
    return (
      <Paper zDepth={1} style={{marginTop: 15}}>
        <Tabs>
          <Tab label="Todos">
            <div>
              {items.map((item, i) => 
                <TodoItem onCheck={(e) => onCheck(item.id) } handleCollaps={this.handleOpenItem} openedItemId={this.state.openedItemId} key={item.id} item={item} />
              )}
            </div>
          </Tab>
          <Tab label="Pendentes">
            <div>
              {items.filter(item => !item.concluido).map((item, i) =>
                <TodoItem onCheck={(e) => onCheck(item.id) } handleCollaps={this.handleOpenItem} openedItemId={this.state.openedItemId} key={item.id} item={item} />
              )}
            </div>
          </Tab>
          <Tab label="Feitos">
            <div>
              {items.filter(item => item.concluido).map((item, i) =>
                <TodoItem onCheck={(e) => onCheck(item.id) } handleCollaps={this.handleOpenItem} openedItemId={this.state.openedItemId} key={item.id} item={item} />
              )}
            </div>
          </Tab>
        </Tabs>
      </Paper>
    )
  }
}

export default ListTodoItems
