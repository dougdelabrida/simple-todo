import React, { Component } from 'react'
import {MuiThemeProvider} from 'material-ui'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import AddItem from './AddItem'
import ListTodoItems from './ListTodoItems'
import injectTapEventPlugin from 'react-tap-event-plugin'
import './App.css'

injectTapEventPlugin()

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#66696f',
    primary1Color: '#76c043',
    accent1Color: '#446f26'
  }
});

class App extends Component {
  state = {
    items: [
      {
        id: 3,
        nome: 'Implementar tela',
        concluido: false,
        image: ''
      },
      {
        id: 2,
        nome: 'Fazer deploy',
        concluido: true
      },
      {
        id: 1,
        nome: 'Checar E-mails',
        concluido: false
      }
    ]
  }
  
  handleAddItem = (item) => {
    const {items} = this.state
    items.unshift({
      id: items[0].id + 1,
      nome: item.nome,
      concluido: false,
      imagem: item.imagem
    })
    this.setState({items})
  }
  
  handleItemNameInput = (itemNameInput) => this.setState({itemNameInput})
  
  handleCheckItem = (id) => {
    const {items} = this.state
    items.map(item => {
      if(item.id === id) {
        item.concluido = !item.concluido
        this.setState({items})
      }
      return item
    })
  }
  
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
        <div className="App">
          <div className="App-header">
            <img src='http://strider.ag/wp-content/uploads/2016/10/logo-strider-white.png' alt="logo"/>
          </div>
          <div className='container'>
            <AddItem onChange={(item) => this.handleAddItem(item)}/>
            <ListTodoItems items={this.state.items} onCheck={this.handleCheckItem}/>            
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default App
